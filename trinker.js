const people = require("./people");

module.exports = {
    title: function (){
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },

    line: function(title = "="){
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },

    allMale: function(p){
        return p.filter((person) => person.gender == 'Male')
        // const toutlesmales = [];
        // for(let i = 0; i < p.length; i++){
            
        //     if( p[i].gender === "Male"){
        //         toutlesmales.push(p[i]);
        //     }
        // }        
        // return toutlesmales;
    },

    allFemale: function(p){
        return p.filter((person) => person.gender == 'Female')
    },

    nbOfMale: function(p){
        return this.allMale(p).length
    },

    nbOfFemale: function(p){
        return this.allFemale(p).length
    },

    nbOfMaleInterest: function(p){
        return p.filter(person => person.looking_for == 'M').length
    },

    nbOfFemaleInterest: function(p){
        return p.filter(person => person.looking_for == 'F').length
    },

    nbGagnePlus: function(p){
        return p.filter(person => parseInt(person.income.substring(1)) > 2000).length
    },

    LoveDrama: function(p){
    return p.filter(person => person.pref_movie.includes("Drama")).length
    },

    LoveSF: function(p){
        return p.filter(

            person => person.pref_movie.includes("Sci-Fi") && person.gender == 'Female').length
    },

    //LEVEL 2

    LoveDocEtGagne: function(p){
        return p.filter(
            person => person.pref_movie.includes("Documentary") && parseInt(person.income.substring(1)) > 1482).length
    },
    
    ListGagnPlus: function(p){
        const list = [];
        let cadre = p.filter(person => parseInt(person.income.substring(1)) > 4000)
                cadre.forEach(element => {
                    list.push(element.id + " " + element.first_name + " " + element.last_name + " " + element.income) 
                });    
        return list
    },

    HlePlusRiche: function(p){
        p = this.allMale(p)
        p.sort((a, b) =>   parseFloat(b.income.substring(1)) - parseFloat(a.income.substring(1)));
            return p[0].first_name + " " + p[0].id
    },

    SalairMoyen: function(p){
        let sum = 0;
        for (let i = 0; i < p.length; i++) {
            sum += parseFloat(p[i].income.substring(1));
    }
        return sum / p.length;
},


    SalairMedian: function(p){
    let array = []
    p.map(function(v) {
        const money = parseFloat(v.income.substring(1));
        array.push({argent : money})
    })

        const orderMoney = array.sort(function(a, b) {
             a.argent - b.argent;
        });
            return orderMoney[array.length / 2]
    },

    NbHnord: function(p){
        return p.filter((person) => person.latitude > 0).length
    },
    
    SalairMoyenHsud: function(p){
        const sudPeople = p.filter((person) => person.latitude < 0)
            let salaire = []
         sudPeople.map(function(e) {
            
            const money = parseFloat(e.income.substring(1))
            salaire.push(money)
             }
        )
        const total = salaire.reduce((a , b) => a + b) / salaire.length
        return  Math.round(total * 100)/ 100
        
    },

    cable: function (p) {
        let tab = []
        let min=0;
        let latlong = p.filter((person) => person.id == 61)[0]
        for (let i = 0; i < p.length; i++) {
            if ( p[i].id != latlong.id){
                tab.push(this.getDistanceFromLatLonInKm(latlong.latitude, latlong.longitude, p[i].latitude, p[i].longitude))
            }
        }
         return tab.sort()
      },
    
        getDistanceFromLatLonInKm: function (lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
        var dLon = this.deg2rad(lon2-lon1); 
        var a = 
          Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) * 
          Math.sin(dLon/2) * Math.sin(dLon/2)
          ; 
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        var d = R * c; // Distance in km
        return d;
      },
      
      deg2rad: function (deg) {
        return deg * (Math.PI/180)
      },
      
    // PresDeBerenice: function(p){

    // },

    
    match: function(p){
        return "not implemented".red;
    }
}

